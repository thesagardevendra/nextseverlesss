import mssql from 'mssql'
import { useState,useEffect } from 'react';
// import { useEffect } from 'react/cjs/react.production.min';

export default function Home({jsonstring}) {
const [data,setData]=useState([])
useEffect(()=>{
  setData(JSON.parse(jsonstring));
},[])
  return (
   <div>
     {data.map((item, index) => {
       return(
          <div key={index}>
            <h1>{item.FIRST_NAME}{item.LAST_NAME}</h1>
          </div>
       )
     })}
     </div>
  )
}

export async function getServerSideProps(){

   const config=()=>({
    server:"125.99.79.17",
    database:"police_quarters",
    user:"admin",
    password:"gis.12345678",
    options: {
        encrypt: false,
        enableArithAbort: false
    },
   })

   const pool = new mssql.ConnectionPool(config());
        await pool.connect();
        const result = await pool.request().query(`SELECT * FROM OFFICER_TABLE_`);
        const quaters = result.recordset;
        const jsonstring=JSON.stringify(quaters);
   return{
    props:{jsonstring}
  }
}

// #localip 192.168.1.131
// SQL_DRIVER = SQL SERVER
// SQL_SERVER =125.99.79.17 
// SQL_DATABASE= police_quarters
// SQL_UID = admin
// SQL_PWD = gis.1234567